INTRODUCTION
------------

*Base Field Display Configurability Override* lets the site builder enforce entity base field visibility in field UI.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/dakwamine/3123757

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/3123757

REQUIREMENTS
------------

This module has been developed on Drupal core 8.8+. May not work on older versions.

 INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

 * Go to **/admin/structure/base-field-display-override/manage**.
 * Configure and save.

Base field overrides will apply upon form submit.
