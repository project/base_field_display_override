<?php

namespace Drupal\base_field_display_override\Form;

use Drupal\base_field_display_override\Service\BaseFieldDisplayOverrideManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BaseFieldDisplayOverrideForm.
 */
class BaseFieldDisplayOverrideForm extends ConfigFormBase {

  /**
   * Base field display override manager service.
   *
   * @var \Drupal\base_field_display_override\Service\BaseFieldDisplayOverrideManagerInterface
   */
  protected $baseFieldDisplayOverrideManager;

  /**
   * Entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->baseFieldDisplayOverrideManager = $container->get('base_field_display_override.manager');
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      BaseFieldDisplayOverrideManagerInterface::CONFIG__OVERRIDES,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'base_field_display_override_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(BaseFieldDisplayOverrideManagerInterface::CONFIG__OVERRIDES);

    // Get available content entities with Field UI.
    $overridableEntityTypes = $this->baseFieldDisplayOverrideManager->getOverridableContentTypes();

    $visibleLabel = $this->t('<span style="color:#0a625c; font-weight:bold;">Visible</span>');
    $noOverrideLabel = $this->t('<span style="color:black; font-weight:bold;">No override</span>');
    $hiddenLabel = $this->t('<span style="color:#a42b33; font-weight:bold;">Hidden</span>');

    foreach ($overridableEntityTypes as $entityTypeId => $overridableEntityType) {
      $wrapperId = $entityTypeId;
      $form[$wrapperId] = [
        '#type' => 'details',
        '#title' => $overridableEntityType->getLabel(),
      ];

      // Gets the base field definitions for a content entity type.
      $baseFieldDefinitions = $this->entityFieldManager->getBaseFieldDefinitions($overridableEntityType->id());

      // For now, this module supports only a single type of override.
      // If necessary, more types can be added using $valueType.
      $valueType = 'display';
      $form[$wrapperId][$valueType] = [
        '#title' => $this->t('Visibility in entity view displays'),
        '#type' => 'fieldset',
      ];

      // Load the field override values from config.
      $fieldOverrides = $config->get($valueType);

      foreach ($baseFieldDefinitions as $key => $baseFieldDefinition) {
        // Insert a radios element for each base field.
        $defaultValue = empty($fieldOverrides[$entityTypeId][$key]) ? BaseFieldDisplayOverrideManagerInterface::CONFIG_VALUE__DISPLAY__NO_OVERRIDE : $fieldOverrides[$entityTypeId][$key];

        $form[$wrapperId][$valueType][$valueType . '+' . $entityTypeId . '+' . $key] = [
          '#description' => $this->t('@fieldDescriptionCurrent value is: <em>@currentValue</em>.', [
            '@currentValue' => $baseFieldDefinition->isDisplayConfigurable('view') ? $visibleLabel : $hiddenLabel,
            '@fieldDescription' => $baseFieldDefinition->getDescription() ? $this->t('@description<br/>', ['@description' => $baseFieldDefinition->getDescription()]) : '',
          ]),
          '#default_value' => $defaultValue,
          '#options' => [
            BaseFieldDisplayOverrideManagerInterface::CONFIG_VALUE__DISPLAY__HIDDEN => $hiddenLabel,
            BaseFieldDisplayOverrideManagerInterface::CONFIG_VALUE__DISPLAY__NO_OVERRIDE => $noOverrideLabel,
            BaseFieldDisplayOverrideManagerInterface::CONFIG_VALUE__DISPLAY__VISIBLE => $visibleLabel,
          ],
          '#title' => $this->t('<strong>@baseFieldName</strong> (<code>@machineName</code>)', [
            '@baseFieldName' => $baseFieldDefinition->getLabel(),
            '@machineName' => $key,
          ]),
          '#type' => 'radios',
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();

    // Parse the values for each available entity type.
    $overridableEntityTypes = $this->baseFieldDisplayOverrideManager->getOverridableContentTypes();

    $valuesToSetInConfig = [];

    foreach ($values as $formKey => $value) {
      $parts = explode('+', $formKey);

      if (count($parts) !== 3) {
        // Not a form value to process.
        continue;
      }

      $valueType = $parts[0];
      $entityTypeId = $parts[1];
      $fieldKey = $parts[2];

      if ($valueType !== 'display') {
        // Hardcoded filter on single value type (see buildForm(...)).
        // Enhance it only when needed.
        continue;
      }

      if (empty($overridableEntityTypes[$entityTypeId])) {
        // Not a valid entity type.
        continue;
      }

      $valuesToSetInConfig[$valueType][$entityTypeId][$fieldKey] = $value;
    }

    $config = $this->config(BaseFieldDisplayOverrideManagerInterface::CONFIG__OVERRIDES);

    // Hard reset the config. It's better to rebuild it completely.
    $config = $config->delete();

    foreach ($valuesToSetInConfig as $valueType => $valuesToSet) {
      $config->set($valueType, $valuesToSet);
    }

    $config->save();

    // Immediately clear the base field definitions cache to force rebuild it.
    $this->entityFieldManager->clearCachedFieldDefinitions();
  }

}
