<?php

namespace Drupal\base_field_display_override\Service;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Class BaseFieldDisplayOverrideManager.
 */
class BaseFieldDisplayOverrideManager implements BaseFieldDisplayOverrideManagerInterface {

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new BaseFieldDisplayOverrideManager object.
   */
  public function __construct(
    EntityFieldManagerInterface $entityFieldManager,
    EntityTypeManagerInterface $entityTypeManager) {
    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeOriginalFieldDefinitions(ContentEntityTypeInterface $entityType) {
    if ($entityType instanceof FieldableEntityInterface) {
      // FieldableEntityInterface instances have the baseFieldDefinitions class.
      return $entityType::baseFieldDefinitions($entityType);
    }

    // If a generic ContentEntityTypeInterface was given, try to load the static
    // method using the original class value.
    $originalClassName = $entityType->getOriginalClass();

    if (method_exists($originalClassName, 'baseFieldDefinitions')) {
      // Assume this is a valid entity type.
      return $originalClassName::baseFieldDefinitions($entityType);
    }

    // Cannot retrieve base field definitions.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getOverridableContentTypes() {
    $entityTypes = [];

    foreach ($this->entityTypeManager->getDefinitions() as $entityTypeId => $entityType) {
      if ($entityType->get('field_ui_base_route') && $entityType->hasViewBuilderClass()) {
        // This should filter on only ContentEntityTypeInterface instances.
        $entityTypes[$entityTypeId] = $entityType;
      }
    }

    return $entityTypes;
  }

}
