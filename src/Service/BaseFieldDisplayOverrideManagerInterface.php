<?php

namespace Drupal\base_field_display_override\Service;

use Drupal\Core\Entity\ContentEntityTypeInterface;

/**
 * Interface BaseFieldDisplayOverrideManagerInterface.
 */
interface BaseFieldDisplayOverrideManagerInterface {

  const CONFIG__OVERRIDES = 'base_field_display_override.overrides';

  const CONFIG_VALUE__DISPLAY__NO_OVERRIDE = 'none';
  const CONFIG_VALUE__DISPLAY__VISIBLE = 'visible';
  const CONFIG_VALUE__DISPLAY__HIDDEN = 'hidden';

  /**
   * Gets entity types suitable for base field display override.
   *
   * @return \Drupal\Core\Entity\ContentEntityTypeInterface[]
   *   Array of entity types.
   */
  public function getOverridableContentTypes();

  /**
   * Gets the original field definitions of a given content entity type.
   *
   * This is useful to workaround the standard entity field service which
   * returns processed base definitions, not original ones.
   *
   * @param \Drupal\Core\Entity\ContentEntityTypeInterface $entityType
   *   The content entity type to get the field definitions.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]|null
   *   An array of field definitions. May be empty. Returns NULL if the entity
   *   type does not support FieldableEntityInterface.
   */
  public function getEntityTypeOriginalFieldDefinitions(ContentEntityTypeInterface $entityType);

}
